import argparse
import json
import os
from pathlib import Path
import re
import time
from urllib.parse import urlparse
from urllib.request import urlretrieve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

preferred_mimetype = 'video/mp4'
parser = argparse.ArgumentParser(prog='mediasite_download',
                                 description='Download videos from mediasite.')
parser.add_argument('urls', metavar='URL', nargs='+', help='a mediasite URL')
parser.add_argument('-username',
                    help='your kerberos username (overrides -auth)')
parser.add_argument('-password',
                    help='your kerberos password (overrides -auth)')
parser.add_argument(
    '-auth',
    help='path to your credentials file (default: ~/.mediasite)',
    default=os.path.join(str(Path.home()), '.mediasite'))
args = parser.parse_args()

if args.username is None or args.password is None:
    cred_file: dict[str, str] = json.load(open(args.auth, 'r'))
    if args.username is None:
        args.username = cred_file['username']
    if args.password is None:
        args.password = cred_file['password']


def set_chrome_options() -> Options:
    """Sets chrome options for Selenium.
    Chrome options for headless browser is enabled.
    """
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_prefs = {}
    chrome_options.experimental_options['prefs'] = chrome_prefs
    chrome_prefs['profile.default_content_settings'] = {'images': 2}
    return chrome_options


def do_login(driver: webdriver.Remote):
    if not 'login' in driver.current_url.lower():
        # skip if not already logged in
        return

    userinput = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, 'UserName')))
    userinput.send_keys(args.username)

    passinput = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, 'Password')))
    passinput.send_keys(args.password)
    passinput.send_keys(Keys.RETURN)

    time.sleep(0.5)
    if 'login' in driver.current_url.lower():
        # raise if still not logged in after logging in
        error = WebDriverWait(driver, 0.5).until(
            EC.presence_of_element_located(
                (By.CSS_SELECTOR, '.validation-summary-errors')))
        if error:
            raise Exception(f'Login failed: {error.text.strip()}')
        else:
            raise Exception(
                'An unknown error happened and we are still on the login page!'
            )


def get_player_options(driver: webdriver.Remote, mediasite_url: str) -> dict:
    driver.get(mediasite_url)

    do_login(driver)

    print(
        'Waiting 1 second because sometimes a bug happens here with the playbutton explicit wait!'
    )
    time.sleep(1)
    play_button = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(
            (By.CSS_SELECTOR, '.playable-overlay .play-arrow')))
    play_button.click()
    print(
        'Waiting 5 seconds until video is buffering and the playeroptions request happened'
    )
    time.sleep(5)

    browser_log = driver.get_log('performance')
    events = [process_browser_log_entry(entry) for entry in browser_log]
    playerinfo = next(
        (event for event in events if 'GetPlayerOptions' in event.get(
            'params', {}).get('response', {}).get('url', '')), None)
    if playerinfo is None:
        raise Exception('PlayerOptions Request not found!')
    req_id = playerinfo.get('params', {}).get('requestId')
    if req_id is None:
        raise Exception('PlayerOptions request ID not found!')
    request_data = driver.execute_cdp_cmd('Network.getResponseBody',
                                          {'requestId': req_id})
    unparsed_body = request_data.get('body')
    if unparsed_body is None:
        raise Exception('PlayerOptions request has no response body!')
    return json.loads(unparsed_body)


def process_browser_log_entry(entry: dict[str, str]):
    response = json.loads(entry['message'])['message']
    return response


def try_deescape(item: str) -> str:
    return item.replace('\\/', '/')


def sanitize_name(name: str) -> str:
    return re.sub(r'[^\w\-_\. ]', '_', name)


def get_download_url(driver: webdriver.Remote, mediasite_url: str) -> tuple[str | str] | None:
    try:
        playerinfo = get_player_options(driver, mediasite_url)

        presentation: dict = playerinfo.get('d', {}).get('Presentation', {})
        title: str = presentation.get('Title', 'UNNAMED')
        try:
            with open('{}.json'.format(sanitize_name(title)), 'w') as f:
                f.write(json.dumps(playerinfo))
        except Exception as e:
            print('Failed to dump player options to file: {}'.format(e))
        description = presentation.get('Description', 'NO DESCRIPTION')
        print('Found {} ({})'.format(title, description))
        videoUrls = next(iter(presentation.get('Streams', [])),
                         {}).get('VideoUrls', [])
        videoUrls = dict((try_deescape(video.get('MimeType')),
                          try_deescape(video.get('Location')))
                         for video in videoUrls)
        print('Found {} video urls:'.format(len(videoUrls)))
        for mimetype in videoUrls:
            print('{:<25}{}'.format(mimetype, videoUrls[mimetype]))
        if preferred_mimetype in videoUrls:
            print('Preferred URL found!')
            return title, videoUrls[preferred_mimetype]
        else:
            raise Exception(
                'Preferred mimetype not available as downloadable stream!')
    except Exception as e:
        print('Failed to get download url {}:\n\t{}'.format(mediasite_url, e))
        return None


def download_file(title: str, direct_url: str) -> None:
    try:
        extension = os.path.splitext(urlparse(direct_url).path)[1]
        filename = sanitize_name(title) + extension
        command = 'curl -L -C - -o "{}" "{}"'.format(filename, direct_url)
        print('Use "{}" to manually download the lecture.'.format(command))
        if Path(filename).exists():
            print(f'File {filename} already exists, not donwloading again!')
        else:
            print(f'Downloading file {filename} now...')
            urlretrieve(direct_url, filename)
    except Exception as e:
        print('Failed to download {}:\n{}'.format(title, e))


def check_url(url: str) -> str | None:
    urlcheck = re.match(
        r'(?:https?:\/{2})?webcast\.tuhh\.de\/Mediasite\/Play\/(\w+)\/?', url)
    if urlcheck:
        view_key = urlcheck.group(1)
        print('Found viewkey: {}'.format(view_key))
        return url
    else:
        print('Invalid mediasite url: {}!'.format(url))
        return None


driver = None
try:
    chrome_options = set_chrome_options()
    caps = DesiredCapabilities.CHROME
    caps['goog:loggingPrefs'] = {'performance': 'ALL'}
    valid_urls = list(url for url in (check_url(url) for url in args.urls)
                      if not url is None)
    if len(valid_urls) == 0:
        raise Exception('No valid url given!')
    try:
        driver = webdriver.Chrome(options=chrome_options,
                                  desired_capabilities=caps)
    except FileNotFoundError as e:
        raise Exception(
            'Chromedriver could not be initialized. Please check your chromedriver installation!'
        )
    videos = dict(video for video in (get_download_url(driver, url)
                                      for url in valid_urls)
                  if not video is None)
    driver.quit()
    for title in videos:
        download_file(title, videos[title])
finally:
    if not driver is None:
        driver.quit()