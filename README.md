# MediaSite Downloader

Python tool that allows you to download lectures from MediaSite using Selenium Web as a Scraper.

## Prerequisites

Of course you need to install `python` (Python 3). 
Additionally you must install selenium and its dependencies. For this you can simply run:

```bash
pip install -r requirements.txt
```

__IMPORTANT__: This script requires the chromedriver for selenium which you can download [here](https://chromedriver.chromium.org/downloads). I'm not exactly sure if necessary but I think it must match your installed chrome version (you can check your version at `chrome://version` in your chrome browser).

## Running

You can simply run the script using:

```bash
python mediasite_download.py
```

The call will of course fail, since you must specify your call parameters. Should be quite simple: you specify one or more mediasite urls that you want to download.

```
usage: mediasite_download [-h] [-username USERNAME] [-password PASSWORD] [-auth AUTH]
                          URL [URL ...]

Download videos from mediasite.

positional arguments:
  URL                 a mediasite URL

optional arguments:
  -h, --help          show this help message and exit
  -username USERNAME  your kerberos username (overrides -auth)
  -password PASSWORD  your kerberos password (overrides -auth)
  -auth AUTH          path to your credentials file (default: ~/.mediasite)
```

## Credentials

### CLI Login

If login is necessary (for private lectures) you can specify your username and password as command line arguments as seen in usage (`mediasite_download.py -h`).

### Auth file

If you don't want to explicitly write your password on every call you can use the auth file.

If no credentials (or partial) are given and a login is required the script will attempt to get your credentials from the `.mediasite` file in your user's home directory which is a simple json file of the following format:

```json
{
	"username": "abc1234",
	"password": "supersecret"
}
```

## TODO

- Mute lecture when scraping

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Authors

Developed by [Michael Chen](@chenmichael).

## Notes

Please don't hesitate to message me for feature requests and feel free to create issues in gitlab. If there is need for features i will try my best to implement them. This is not in any way a good codebase (yet) so don't expect any enterprise scripting here.